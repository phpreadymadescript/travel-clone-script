
Quick overview:
This document is prepared by DOD IT Solutions, to give you an idea of how our MAKE MY TRIP HOTEL CLONE SCRIPT features would be.Hotel Clone Script is completely in built and has automated modules which are specific for online travel business. Online web based benefits of hotel room reservation process, create more sale leads and track customers, and most of all, it helps in maintaining your repeated customers info. You can analyze reservation trend details and work towards your future needs.


UNIQUE FEATURES:
Room/Inventory availability
Real Time Pricing
Real Time Booking/Reservation
Online Cancellation/Refund
Booking Amendments
Travel Advisory Services
Destination Management
White Label Solutions
Affiliate Model
Multiple Payment Options
Easy to manage Property Information
Room Type , Inventory
Room Rate Amenities
Seasonal Rate and Other Charges

COMMON  FEATURES SEARCH OPTIONS
User can search by entering location, check-in and check-out date along with the number of rooms and as well the number of adults/children whose going to reside.
Map search is also available.

FILTER OPTIONS
User can check by selecting the stars (either 3 star hotels or 5 star hotels...etc)
Check by filtering the pay scale ranging from the least amount to the highest.
Can check by selecting a particular location.
HOTEL  INFO
User can view the complete address of the selected hotel.
Can view images and hotel amenities.
Choose the particular room that you wish (Singe/Double/Luxury/Deluxe...etc).
Access to API Hotels and Offer Hotels.

MAKE MY TRIP HOTEL ADMIN PANEL LOGIN
Log in using desired username and password  (provided by admin)

PROFILE
View the Agent details
Edit the agent details, block and unblock the agent record
Change the login Password

HOTEL MANAGEMENT
Add the trip details
Searched by trip details using by  hotel type, from the city, and status
List the  trip details  using on load Ajax functions
Manage the trip details (Edit, delete, block and unblock, pagination)
View and manage details
View the particular hotel details
structures designed by drag and drop method
Can add/delete hotel details.
Manage bookings and tickets list.
Manage image uploads.
Manage rooms.
Manage user info.
Maintain payment gateway and user wallet.
Manage day’s offers.
Complete end-to-end access.
HOTEL IMAGES AND VIDEOS
Add the hotel images
Manage the hotel images(edit, status,delete)
Upload the hotel videos

PASSENGER MANAGEMENT
List the booked traveler details
View the particular guest details
View the booking details
View the particular count and view the hotel details
Searched by hotel details using ticket no , booked date, user type, from city , to city

SEAT MANAGEMENT
Search and then list the seat details
Searched by seat details using date

TRAVELER MANAGEMENT
List the all details
View the particular traveler details
Filtered by traveler details using date,from to city

CANCEL
List all traveler
View the particular details
Filtered by tickets using ticket no, cancel date, travel date

PAYMENT MANAGEMENT
List the hotel details
View the particular payment transaction details
Filtered by trip details using hotel type, from city, to city , date

CANCELLATION POLICIES
Add the refund status
Manage the details(edit,status,delete)

SMS LOG DETAILS
List the sms details
Manage the details(delete)
Search sms details by bus and day,month,year

EMAIL LOG DETAILS
List the email details
Manage the details(delete)
Search details details by bus and day,month,year

MANAGE BANNERS
Banners to pop-up with offers would be generated.

MANAGE MARQUEE TEXT
Texts as flash notes either at the top or bottom of the page could be generated.

Check Out Our Product in:

https://www.doditsolutions.com/makemytrip-clone/

http://scriptstore.in/product/asp-net-makemytrip-clone/

http://phpreadymadescripts.com/shop/make-my-trip-hotel-clone-script.html
